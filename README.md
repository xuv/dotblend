# dotblend

These file are part of the #dotblend.  
You can find more info on the project at http://dotblend.tumblr.com

## Rules of #dotblend

 1. Use default startup scene only (1 cube, 1 camera, 1 lamp).
 2. No mesh editing of any kind (aka. Don’t use Edit Mode).
 3. No object moving, rotating or scaling (no transforms).
 4. Square render.
 5. Always share a packed blend file with it.
 6. Tag it #dotblend.
 7. Publish early, publish often.

## Credits
+ Copyright: Julien Deswaef
+ Licence: GPL 
